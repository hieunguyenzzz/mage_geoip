<?php

namespace Mage\GeoIp\Plugin;

use Mage\GeoIp\Helper\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

class Redirect
{

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;
    /**
     * @var Config
     */
    private $config;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var ResultFactory
     */
    private $resultFactory;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        Config $config,
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        ResultFactory $resultFactory
    )
    {
        $this->request = $request;
        $this->config = $config;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Get Helper.
     *
     * @return Config
     */
    protected function getHelper()
    {
        return $this->config;
    }


    public function afterRenderResult(\Magento\Framework\Controller\ResultInterface $subject, $result)
    {
        $storeId = $this->getHelper()->getStoreMapped();
        $storeManager = $this->getHelper()->getStoreManager();
        $identifier = trim($this->request->getPathInfo(), '/');
        try {
            if ($this->getHelper()->isEnabled() && $storeManager->getStore()->getId() != $storeId) {
                $url = $this->getHelper()->getStoreUrl($storeId) . $identifier;
                header('Location: ' . $url, true, 301);
                exit;
            }
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
        }

        return $result;
    }
}