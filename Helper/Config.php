<?php
/**
 * @author Hieu Nguyen
 * @package Mage_GeoIP
 */

namespace Mage\GeoIp\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Store\Model\StoreManagerInterface;

class Config
{
    const CONFIG_IS_ACTIVE = 'geoip/general/active';
    const COUNTRY_MAPPED = 'geoip/general/country_list';
    const EXLUDE_IP = 'geoip/general/ip_exclude';

    /**
     * @var State
     */
    protected $_state;

    /**
     * @var ScopeConfigInterface
     */
    protected $_config;
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var Request
     */
    private $request;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param State $state
     * @param Country $maxMindCountry,
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        State $state,
        StoreManagerInterface $storeManager,
        RemoteAddress $remoteAddress,
        Request $request
    )
    {
        $this->_state = $state;
        $this->_config = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->remoteAddress = $remoteAddress;
        $this->request = $request;
    }

    /**
     * @return bool
     */
    public function isEnabled(){
        $active = (bool)(int)$this->_config->getValue(self::CONFIG_IS_ACTIVE);
        $ipExclude = $this->_config->getValue(self::EXLUDE_IP);
        $ip = $this->remoteAddress->getRemoteAddress();
        if (!$active) {
            return false;
        }

        if (in_array($ip, explode(';', $ipExclude))) {
            return false;
        }

        if (!$this->getCountryCode()) {
            return false;
        }

        if (!$this->getStoreMapped()) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->request->getServerValue('HTTP_CF_IPCOUNTRY');
    }

    /**
     * @return null|int
     */
    public function getStoreMapped()
    {
        $currentCountry = $this->getCountryCode();
        /** @var \Magento\Store\Model\Store $store */
        foreach ($this->_storeManager->getStores() as $store) {
            if ($store->isActive()) {
                $country = $this->_config->getValue(self::COUNTRY_MAPPED, 'store', $store->getCode());
                $country = explode(',', $country);
                if (in_array($currentCountry, $country)) {
                    return $store->getId();
                }
            }
        }
        return null;
    }

    /**
     * @return StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->_storeManager;
    }

    /**
     * @param int $storeId
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreUrl($storeId)
    {
        return $this->_storeManager->getStore($storeId)->getBaseUrl(); // Store mapped url;
    }
}
